import pytest

from pages.login_result import IntramLoginResult
from pages.login import IntramLoginPage

email_address = "jolivehodehou@yahoo.com"
password = "7AAdZT3Lq2pepZg"
invalid_email = "joli@.com"
email_nonexistent = "jolivehodehou7@gmail.com"



def test_login_invalid_email(browser):

    login_page = IntramLoginPage(browser)
    login_result_page = IntramLoginResult(browser)

    login_page.load()
    login_page.login(invalid_email, password)

    assert "error-box" in login_result_page.invalid_mail_value()


def test_login_user_not_exist(browser):

    login_page = IntramLoginPage(browser)
    login_result_page = IntramLoginResult(browser)

    login_page.load()
    login_page.login(email_nonexistent, password)

    assert "alert alert-warning ml-3 mr-3 pt-2 pb-2" in login_result_page.email_nonexistent_value()
    

def test_login_required_email(browser):

    login_page = IntramLoginPage(browser)
    login_result_page = IntramLoginResult(browser)

    login_page.load()
    login_page.login("", password)

    assert "error-box" in login_result_page.required_email_value()

def test_login_required_password(browser):

    login_page = IntramLoginPage(browser)
    login_result_page = IntramLoginResult(browser)

    login_page.load()
    login_page.login(email_address, "")

    assert "error-box" in login_result_page.required_password_value()


def test_login_required_email_and_password(browser):

    login_page = IntramLoginPage(browser)
    login_result_page = IntramLoginResult(browser)

    login_page.load()
    login_page.login("", "")

    assert "error-box" in login_result_page.required_email_value()
    assert "error-box" in login_result_page.required_password_value()

    
